FROM nginx:1.21.6-alpine

COPY . /usr/share/nginx/html
RUN sed -i 's/    listen       80;/    listen       11130;/g' /etc/nginx/conf.d/default.conf && \
    sed -i 's/    listen  [::]:80;/    listen  [::]:11130;/g' /etc/nginx/conf.d/default.conf

EXPOSE 11130/tcp
